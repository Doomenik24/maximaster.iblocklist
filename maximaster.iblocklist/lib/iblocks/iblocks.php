<?php
namespace Maximaster\IblockList\Iblocks;

use Bitrix\Main\Localization\Loc;
use Bitrix\Iblock\IblockTable;

Loc::loadMessages(__FILE__);

/**
 * Модель инфоблоков.
 */
class IblocksTable extends IblockTable
{
    /**
     * {@inheritdoc}
     */

    public static function getTableName()
    {
        return 'b_iblock';
    }

    public static function getMap()
    {
        /*
            Причина, из-за которой было перезаписано поле ACTIVE:
            https://github.com/DigitalWand/digitalwand.admin_helper/issues/19
        */
        return array_merge(parent::getMap(), [
            'ACTIVE' => [
                'data_type' => 'string',
                'values' => array('N', 'Y'),
                'title' => Loc::getMessage('ACTIVE')
            ]
        ]);
    }
}