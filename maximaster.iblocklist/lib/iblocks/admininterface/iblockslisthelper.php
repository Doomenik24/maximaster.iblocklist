<?php
namespace Maximaster\IblockList\Iblocks\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminListHelper;
use Bitrix\Main\Localization\Loc;

/**
 * Хелпер описывает интерфейс, выводящий список инфоблоков.
 *
 * {@inheritdoc}
 */
class IblocksListHelper extends AdminListHelper
{
    protected static $model = '\Maximaster\IblockList\Iblocks\IblocksTable';

    /* Переопределяем метод, чтобы не показывать кнопку "Создать элемент" */
    protected function getContextMenu()
    {

    }
    /* Переопределением метода убираем лишний код и добавляем свою ссылку на редактирование ИБ*/
    protected function getRowActions($data, $section = false)
    {
        $actions = array();
        $lAdmin = new CAdminList();

        $viewQueryString = 'module=' . static::getModule() . '&view=' . static::getViewName() . '&entity=' . static::getEntityCode();

        if ($this->hasWriteRights()) {
            $actions['edit'] = array(
                'ICON' => 'edit',
                'DEFAULT' => true,
                'TEXT' => Loc::getMessage('DIGITALWAND_ADMIN_HELPER_LIST_EDIT'),
                'ACTION' => $lAdmin->ActionRedirect("/bitrix/admin/iblock_edit.php?type=" . $data['IBLOCK_TYPE_ID'] . "&lang=ru&ID=" . $data['ID'])
            );
        }
        if ($this->hasDeleteRights()) {
            $actions['delete'] = array(
                'ICON' => 'delete',
                'TEXT' => Loc::getMessage("DIGITALWAND_ADMIN_HELPER_LIST_DELETE"),
                'ACTION' => "if(confirm('" . Loc::getMessage('DIGITALWAND_ADMIN_HELPER_LIST_DELETE_CONFIRM') . "')) " . $this->list->ActionDoGroup($data[$this->pk()],
                        $section ? "delete-section" : "delete", $viewQueryString)
            );
        }

        return $actions;
    }
}
