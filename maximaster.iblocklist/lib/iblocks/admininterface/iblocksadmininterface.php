<?php
namespace Maximaster\IblockList\Iblocks\AdminInterface;

use Bitrix\Main\Localization\Loc;
use DigitalWand\AdminHelper\Helper\AdminInterface;
use DigitalWand\AdminHelper\Widget\NumberWidget;
use DigitalWand\AdminHelper\Widget\StringWidget;
use DigitalWand\AdminHelper\Widget\CheckboxWidget;

Loc::loadMessages(__FILE__);

/**
 * Описание интерфейса (табок и полей) страницы с списком ИБ в админке.
 *
 * {@inheritdoc}
 */
class IblocksAdminInterface extends AdminInterface
{
    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'MAIN' => [
                'NAME' => Loc::getMessage('MAXIMASTER_INSET_IBLOCK_DATA'),
                'FIELDS' => [
                    'ID' => [
                        'WIDGET' => new NumberWidget(),
                        'READONLY' => true,
                        'FILTER' => true,
                        'HIDE_WHEN_CREATE' => true
                    ],
                    'NAME' => [
                        'WIDGET' => new StringWidget(),
                        'SIZE' => '80',
                        'FILTER' => '%',
                        'REQUIRED' => true
                    ],
                    'SORT' => [
                        'WIDGET' => new NumberWidget(),
                    ],
                    'ACTIVE' => [
                        'WIDGET' => new CheckboxWidget(),
                        'FILTER' => true
                    ],
                    'IBLOCK_TYPE_ID' => [
                        'WIDGET' => new StringWidget(),
                        'HIDE_WHEN_CREATE' => true,
                        'READONLY' => true,
                        'EDIT_IN_LIST' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function helpers()
    {
        return array(
            '\Maximaster\IblockList\Iblocks\AdminInterface\IblocksListHelper',
            '\Maximaster\IblockList\Iblocks\AdminInterface\IblocksEditHelper'
        );
    }
}