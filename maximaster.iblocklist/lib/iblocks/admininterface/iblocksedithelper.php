<?php
namespace Maximaster\IblockList\Iblocks\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminEditHelper;

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования ИБ.
 *
 * {@inheritdoc}
 */
class IblocksEditHelper extends AdminEditHelper
{
    protected static $model = '\Maximaster\AdminHelper\Iblocks\IblocksTable';
}