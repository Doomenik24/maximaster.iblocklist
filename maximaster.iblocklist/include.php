<?php
CModule::AddAutoloadClasses(
    'maximaster.iblocklist',
    [
        'Maximaster\IblockList\Iblocks\AdminInterface\IblocksEditHelper' => 'lib/iblocks/admininterface/iblocksedithelper.php',
        'Maximaster\IblockList\Iblocks\AdminInterface\IblocksListHelper' => 'lib/iblocks/admininterface/iblockslisthelper.php',
        'Maximaster\IblockList\Iblocks\AdminInterface\IblocksAdminInterface' => 'lib/iblocks/admininterface/iblocksadmininterface.php',
        'Maximaster\IblockList\Iblocks\IblocksTable' => 'lib/iblocks/iblocks.php',
    ]
);