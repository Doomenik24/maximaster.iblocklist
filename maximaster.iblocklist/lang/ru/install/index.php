<?php
$MESS = [
    'MAXIMASTER_IBLOCKLIST_MODULE_ID' => 'maximaster.iblocklist',
    'MAXIMASTER_IBLOCKLIST_MODULE_NAME' => 'Список инфоблоков сайта',
    'MAXIMASTER_IBLOCKLIST_MODULE_DESCRIPTION' => 'Данный модуль получить список всех ИБ, которые есть на сайте.
        В разделе "Контент" создается пункт "Список ИБ", при нажатии на который произойдет переход на страницу со списком всех ИБ присутствующих на сайте.
        Для работы данного модуля необходимо установить модуль "digitalwand.admin_helper" -  https://github.com/DigitalWand/digitalwand.admin_helper/',
    'MAXIMASTER_IBLOCKLIST_PARTNER_NAME' => 'ООО Максимастер',
    'MAXIMASTER_IBLOCKLIST_PARTNER_URI' => 'http://www.maximaster.ru'
];