<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class maximaster_iblocklist extends CModule
{
    const MODULE_ID = 'maximaster.iblocklist';

    function __construct()
    {
        $arModuleVersion = [];

        include(__DIR__ . '/version.php');

        $this->MODULE_ID = Loc::getMessage('MAXIMASTER_IBLOCKLIST_MODULE_ID');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage('MAXIMASTER_IBLOCKLIST_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MAXIMASTER_IBLOCKLIST_MODULE_DESCRIPTION');

        $this->PARTNER_NAME = Loc::getMessage('MAXIMASTER_IBLOCKLIST_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('MAXIMASTER_IBLOCKLIST_PARTNER_URI');
    }

    function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);

        Loader::includeModule($this->MODULE_ID);
    }

    function DoUninstall()
    {
        Loader::includeModule($this->MODULE_ID);

        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
}