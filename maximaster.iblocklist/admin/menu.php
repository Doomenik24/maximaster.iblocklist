<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Maximaster\IblockList\Iblocks\AdminInterface\IblocksEditHelper;
use Maximaster\IblockList\Iblocks\AdminInterface\IblocksListHelper;

Loc::loadMessages(__FILE__);

if (!Loader::includeModule('digitalwand.admin_helper') || !Loader::includeModule('maximaster.iblocklist')) {
    return;
}


$arMenu = [
    'parent_menu' => 'global_menu_content',
    'sort' => 100,
    'icon' => 'fileman_sticker_icon',
    'page_icon' => 'fileman_sticker_icon',
    'text' => Loc::getMessage('MAXIMASTER_IBLOCKLIST'),
    'url' => IblocksListHelper::getUrl(),
    'more_url' => array(
        IblocksEditHelper::getUrl(),
    )

];

return $arMenu;